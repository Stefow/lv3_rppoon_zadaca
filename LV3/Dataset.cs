﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3
{
    interface Prototype
    {
        Prototype Clone();
    }
    class Dataset : Prototype
    {

        public List<List<string>> data; //public samo radi provjere
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(Dataset ds)
        {
            data = new List<List<string>>();
            foreach (List<string> InnerList in ds.GetData())
            {
                
                List<string> row = new List<string>();
                foreach (string item in InnerList)
                {
                    string titem = item;
                    row.Add(titem);
                }
                data.Add(row);
            }
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }

        }
        public IList<List<string>> GetData()
        {
            return
           new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }
        public Prototype Clone()
        {
            Prototype dataClone = new Dataset(this);
            return dataClone;
        }
    }

}
