﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset A = new Dataset("Dokument.csv");
            Dataset B;
            Console.WriteLine("\nISPIS A:");
            A.data[0].ForEach(i => Console.Write("{0}\t \n", i));
            B = (Dataset)A.Clone();
            Console.WriteLine("\nISPIS B:");
            B.data[0].ForEach(i => Console.Write("{0}\t \n", i));
            Console.WriteLine("\nISPIS B nakon brisanja A:");
            A.ClearData();
            B.data[0].ForEach(i => Console.Write("{0}\t \n", i));

        }
    }
}
