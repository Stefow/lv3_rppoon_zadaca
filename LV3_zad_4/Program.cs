﻿using System;

namespace LV3_zad_4
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification notification = new ConsoleNotification("Petar", "Alarm", "Nogometna utakmica!", DateTime.Now, Category.ALERT, ConsoleColor.Yellow);
            NotificationManager manager = new NotificationManager();
            manager.Display(notification);
        }
    }
}
