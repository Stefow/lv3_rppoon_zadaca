﻿using System;

namespace LV3_zad_5
{
    class Program
    {

        static void Main(string[] args)
        {
            IBuilder builder = new NotificationBuilder();
            NotificationManager manager = new NotificationManager();
            manager.Display(builder.Build());

        }
    }
}
