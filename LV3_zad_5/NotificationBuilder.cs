﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_zad_5
{
    class NotificationBuilder:IBuilder
    {
        private string Autor = "Stefow";
        private string Title = "error 404";
        private string Text = "its wrong";
        private DateTime Time = DateTime.Now;
        private Category Level = Category.ERROR;
        private ConsoleColor Color = ConsoleColor.Blue;


        public ConsoleNotification Build()
        {
            return new ConsoleNotification(Autor,Title,Text,Time,Level,Color);
        }
        public IBuilder SetAuthor(String author)
        {
            Autor = author;
            return this;
        }
        public IBuilder SetTitle(String title)
        {
            Title = title;
            return this;
        }
        public IBuilder SetTime(DateTime time)
        {
            Time = time;
            return this;
            
        }
        public IBuilder SetLevel(Category level)
        {
            Level = level;
            return this;
        }
        public IBuilder SetColor(ConsoleColor color)
        {
            Color = color;
            return this;
        }
        
        public IBuilder SetText(String text)
        {
            Text = text;
            return this;
        }
    }
}
