﻿using System;
using System.Collections.Generic;
namespace LV3_zad_2
{
    class Program
    {
        static void Main(string[] args)
        {
            RandGeneratorMatrica rga = RandGeneratorMatrica.Instance;
            double[,] matrica = new double[5,5];
            matrica = rga.Dimensions(5, 5);
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.Write(matrica[i,j]+",");
                }
                Console.WriteLine("\n");
            }
        }
    }
}
