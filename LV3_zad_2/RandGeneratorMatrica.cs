﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_zad_2
{
    class RandGeneratorMatrica
    {
        private static RandGeneratorMatrica instance = new RandGeneratorMatrica();
        private Random generator;
        private RandGeneratorMatrica()
        {
            this.generator = new Random();
        }
        public static RandGeneratorMatrica Instance
        {
            get
            {
                return instance;
            }
        }
        public double[,] Dimensions (int red,int stupac)
        {
            double [,] matrica = new double[red,stupac];
            for(int i=0;i<red;i++)
            {
                for (int j = 0; j < stupac; j++)
                {
                    matrica[i, j] = generator.NextDouble();
                }
            }
            return matrica;
        }

    }
}
